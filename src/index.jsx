// import * as $ from "jquery";
import Post from "@models/Post";
import logo from "@/assets/logo.jpg";
import "./babel";
import "@/styles/styles.css";
import "@/styles/vars.scss";

import React from "react";
import { render } from "react-dom";

const post = new Post("Webpack post title", logo);
// $("pre").addClass("code").html(post.toString());

const App = () => {
  return (
    <div className="container">
      <h1>Webpack Course 321</h1>

      <hr />

      <div className="logo"></div>

      <hr />

      <pre></pre>

      <hr />

      <div className="card">
        <h2>SCSS</h2>
      </div>
    </div>
  );
};

render(<App />, document.getElementById("app"));
